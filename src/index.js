import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { BrowserRouter } from 'react-router-dom';
import { store, persistor } from 'mccore/store';
import './assets/scss/base/_base.scss';
import './assets/scss/base/_font.scss';
import './assets/scss/base/_typography.scss';
import MortgageCalculator from 'mcscreens/MortgageCalculator';
import * as serviceWorker from './serviceWorker';

/**
|--------------------------------------------------
| Main app initialization
| Preparing Redux for App with Providers
| Rendering App inside wrapper element
|--------------------------------------------------
*/
render(
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <BrowserRouter>
                <MortgageCalculator />
            </BrowserRouter>
        </PersistGate>
    </Provider>,
    document.getElementById('wrapper')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
