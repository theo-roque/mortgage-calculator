import { applyMiddleware, createStore, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createPromise } from 'redux-promise-middleware';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootSaga from 'mccore/sagas';
import combinedReducers from 'mccore/reducers';

const sagaMiddleware = createSagaMiddleware();

/**
|--------------------------------------------------
| Put reducers to local storage
| Do not store form to local storage
|--------------------------------------------------
*/
const reducer = persistReducer({ key: 'state', storage }, combinedReducers);

const middleware = compose(applyMiddleware(createPromise(), sagaMiddleware));

//  Prepare store for Provider
const store = createStore(reducer, middleware);

//  Mount sagas
sagaMiddleware.run(rootSaga);

//  Prepare persistor for persistStore
const persistor = persistStore(store);

//  Exporting store and persistor for providers in index
export { store, persistor };
