/**
 * Formatting an integer with a comma as a thousands separators
 * @param {string|int} sortedBy 1 - sort object properties by specific value.
 * @returns {String} properly formatted number.
 */
export default function formatNumber(number) {
    number = number.toFixed(2);
    return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}
