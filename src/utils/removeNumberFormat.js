const removeNumberFormat = number => {
    return parseFloat(number.replace(/\$|,/g, ''));
};

export default removeNumberFormat;
