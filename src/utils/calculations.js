/* eslint-disable no-bitwise */

const getInterestRates = interestRate => {
    return (interestRate * 0.01) / 12.0;
};

const getMonthlyLoanDenom = (r, tenureYears) => {
    return (1 + r) ** (tenureYears * 12) - 1;
};

const getMonthlyLoanNomin = (r, tenureYears) => {
    return r * (1 + r) ** (tenureYears * 12);
};

const getMonthlyLoan = (loanAmount, monthyLoanNomin, monthlyLoanDenom) => {
    return loanAmount * (monthyLoanNomin / monthlyLoanDenom);
};

const getInterestAmount = (loanAmount, r) => {
    return loanAmount * r;
};

const getPrincipalAmount = (monthlyLoanTotal, interestAmount) => {
    return monthlyLoanTotal - interestAmount;
};

const getPercentage = (principalAmount, monthlyLoan) => {
    return (principalAmount / monthlyLoan) * 100;
};

export default {
    getInterestRates,
    getMonthlyLoanDenom,
    getMonthlyLoanNomin,
    getMonthlyLoan,
    getInterestAmount,
    getPrincipalAmount,
    getPercentage
};
