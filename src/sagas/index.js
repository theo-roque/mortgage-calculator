import { all } from 'redux-saga/effects';
import { calculatorSaga } from 'mcscreens/MortgageCalculator/MortgageCalculator.saga';

export default function* rootSaga() {
    yield all([...calculatorSaga]);
}
