import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Header, Responsive, Container } from 'semantic-ui-react';
import { connect } from 'react-redux';
import Calculator from 'mccomponents/Calculator';
import BanksListing from 'mccomponents/BanksListing';
import { actions as calculatorActions } from 'mcscreens/MortgageCalculator/MortgageCalculator.reducer';
import './MortgageCalculator.scss';

class MortgageCalculator extends Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    componentDidMount() {
        const { fetchBanksData, fetchAverageInterest } = this.props;
        fetchBanksData();
        fetchAverageInterest();
    }

    render() {
        return (
            <main className="main">
                <Responsive as={Container} textAlign="center" fluid>
                    <Header size="large" className="logo-header">
                        ohmyhome
                    </Header>
                    <Calculator />
                    <BanksListing />
                </Responsive>
            </main>
        );
    }
}

MortgageCalculator.defaultProps = {
    fetchBanksData: () => {},
    fetchAverageInterest: () => {}
};

MortgageCalculator.propTypes = {
    fetchBanksData: PropTypes.func,
    fetchAverageInterest: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
    fetchBanksData: () => {
        return new Promise((resolve, reject) => {
            dispatch({
                promise: { resolve, reject },
                type: calculatorActions.START_FETCHING_BANK_DATA
            });
        }).catch(error => ({ error }));
    },
    fetchAverageInterest: () => {
        return new Promise((resolve, reject) => {
            dispatch({
                promise: { resolve, reject },
                type: calculatorActions.START_FETCHING_AVERAGE_INTEREST
            });
        }).catch(error => ({ error }));
    }
});

export default connect(null, mapDispatchToProps)(MortgageCalculator);
