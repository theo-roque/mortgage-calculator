import { takeLatest, put, call } from 'redux-saga/effects';
// import restClient from 'mccore/api/restClient';
import { actions as calculatorActions } from 'mcscreens/MortgageCalculator/MortgageCalculator.reducer';
import bankRates from 'mccore/test/bankRates.json'; // Temporary JSON response
import averageInterest from 'mccore/test/bankRatesSimplified.json'; // Temporary JSON response

export function* fetchBanksData({ promise }) {
    try {
        // const fetchBanksDataAPI = yield restClient.get(`bankRates`);
        const fetchBanksDataAPI = bankRates; // Temporary!
        yield put({
            type: calculatorActions.FETCHING_BANK_DATA_SUCCESSFUL,
            response: fetchBanksDataAPI.results
        });
        yield call(promise.resolve);
    } catch (error) {
        yield put({
            type: calculatorActions.FETCHING_BANK_DATA_FAILED
        });
        yield call(promise.reject, error.response.data);
    }
}

export function* fetchAverageInterest({ promise }) {
    try {
        // const fetchAverageInterestAPI = yield restClient.get(`bankRates/simplified`);
        const fetchAverageInterestAPI = averageInterest; // Temporary!
        yield put({
            type: calculatorActions.FETCHING_AVERAGE_INTEREST_SUCCESSFUL,
            response: fetchAverageInterestAPI.results[0]
        });
        yield call(promise.resolve);
    } catch (error) {
        yield put({
            type: calculatorActions.FETCHING_AVERAGE_INTEREST_FAILED
        });
        yield call(promise.reject, error.response.data);
    }
}

export function* saveCalculatedData({ promise, formData }) {
    try {
        yield put({
            type: calculatorActions.SAVE_CALCULATED_DATA_SUCCESFUL,
            response: {
                ...formData
            }
        });
        yield call(promise.resolve);
    } catch (error) {
        yield put({
            type: calculatorActions.SAVE_CALCULATED_DATA_FAILED
        });
        yield call(promise.reject, error.response.data);
    }
}

/**
 * Register action to watcher
 */
export const calculatorSaga = [
    takeLatest(calculatorActions.START_FETCHING_BANK_DATA, fetchBanksData),
    takeLatest(calculatorActions.START_SAVE_CALCULATED_DATA, saveCalculatedData),
    takeLatest(calculatorActions.START_FETCHING_AVERAGE_INTEREST, fetchAverageInterest)
];
