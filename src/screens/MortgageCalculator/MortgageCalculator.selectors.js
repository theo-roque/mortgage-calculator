import { createSelector } from 'reselect';

export const getCalculatedData = state => state.calculator.calculatedData;

export const getBanksData = state => state.calculator.banks;

export const getAverageInterest = state => state.calculator.averageInterest;

export default createSelector([getCalculatedData, getBanksData, getAverageInterest]);
