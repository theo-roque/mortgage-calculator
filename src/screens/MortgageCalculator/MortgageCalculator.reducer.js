export const actions = {
    START_FETCHING_BANK_DATA: 'START_FETCHING_BANK_DATA',
    FETCHING_BANK_DATA_SUCCESSFUL: 'FETCHING_BANK_DATA_SUCCESSFUL',
    FETCHING_BANK_DATA_FAILED: 'FETCHING_BANK_DATA_FAILED',

    START_FETCHING_AVERAGE_INTEREST: 'START_FETCHING_AVERAGE_INTEREST',
    FETCHING_AVERAGE_INTEREST_SUCCESSFUL: 'FETCHING_AVERAGE_INTEREST_SUCCESSFUL',
    FETCHING_AVERAGE_INTEREST_FAILED: 'FETCHING_AVERAGE_INTEREST_FAILED',

    START_SAVE_CALCULATED_DATA: 'START_SAVE_CALCULATED_DATA',
    SAVE_CALCULATED_DATA_SUCCESFUL: 'SAVE_CALCULATED_DATA_SUCCESFUL',
    SAVE_CALCULATED_DATA_FAILED: 'SAVE_CALCULATED_DATA_FAILED',

    START_CALCULATE_MORTGAGE: 'START_CALCULATE_MORTGAGE',
    CALCULATE_MORTGAGE_SUCCESFUL: 'CALCULATE_MORTGAGE_SUCCESFUL',
    CALCULATE_MORTGAGE_FAILED: 'CALCULATE_MORTGAGE_FAILED'
};

export const initialState = {
    error: null,
    calculatedData: {},
    banks: [],
    averageInterest: 0
};

export default (state = initialState, { type, response }) => {
    switch (type) {
        case actions.FETCHING_BANK_DATA_SUCCESSFUL: {
            return {
                ...state,
                banks: response
            };
        }
        case actions.FETCHING_AVERAGE_INTEREST_SUCCESSFUL: {
            return {
                ...state,
                averageInterest: parseFloat(response.averageFirstYearInterestRate)
            };
        }
        case actions.SAVE_CALCULATED_DATA_SUCCESFUL: {
            return {
                ...state,
                calculatedData: response
            };
        }
        case actions.CALCULATE_MORTGAGE_FAILED: {
            return {
                ...state,
                error: response
            };
        }
        default:
            return state;
    }
};
