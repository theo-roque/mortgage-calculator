import axios from 'axios';

/**
 * Preparing rest client for react app
 * Here should be all default params defined
 */
// Set the token here since this is only a test (no prod)
const restClient = axios.create({
    baseURL: 'https://cms.staging.ohmyhome.io/techtest/calculator',
    headers: {
        common: {
            'OmhWeb-Auth': 'WU5FvjUt9pTuRPaIuTaD7EOfhNRxfIqOIfSW3I9Rt307MQcQHT6MAajsTY4KPfrE'
        }
    }
});

// handle generic events - like loading and 500 type errors - in API interceptors
restClient.interceptors.request.use(config => {
    // display a single subtle loader on the top of the page when there is networking in progress
    // avoid multiple loaders, use placeholders or consistent updates instead
    return config;
});

restClient.interceptors.response.use(
    resp => {
        return resp;
    },
    err => {
        // if you have no specific plan B for errors, let them be handled here with a notification
        const { status } = err.response;
        if (status === 404) {
            if (err.response.data && !err.response.data.code) {
                err.response.data = {
                    code: 'error404'
                };
            }
        } else if (status === 405) {
            err.response.data = {
                code: 'error405'
            };
        } else if (status >= 500) {
            err.response.data = {
                code: 'error500'
            };
            // const message = data.message || 'Ooops, something bad happened.';
            // console.error(message);
        }

        throw err;
    }
);

export default restClient;
