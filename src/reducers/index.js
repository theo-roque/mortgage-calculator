import { combineReducers } from 'redux';

//  Importing reducers
import { reducer as form } from 'redux-form';
import calculator from 'mcscreens/MortgageCalculator/MortgageCalculator.reducer';

//  Combining all existing reducers
const appReducer = combineReducers({
    form,
    calculator
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
};

export default rootReducer;
