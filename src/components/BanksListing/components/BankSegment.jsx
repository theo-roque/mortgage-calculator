import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Segment, Grid, Image, Progress, Table, Button } from 'semantic-ui-react';
import formatNumber from 'mcutils/formatNumber';
import getOrdinalSuffix from 'mcutils/getOrdinalSuffix';
import './BankSegment.scss';

class BankSegment extends Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    buildTableBody() {
        const {
            calculatedData: { tenureYears, monthlyLoan },
            bank: { interestRatesDetails, interestRates }
        } = this.props;

        const dom = [];
        // eslint-disable-next-line no-plusplus
        for (let i = 0; i < tenureYears; i++) {
            let interestRate = 0;
            let interestDetail = '';
            if (i < interestRates.length) {
                interestRate = interestRates[i];
                interestDetail = interestRatesDetails[i];
            } else {
                interestRate = interestRates[interestRates.length - 1]; // Get the last interest rate for remaining months
                interestDetail = interestRatesDetails[interestRatesDetails.length - 1];
            }
            dom.push(
                <Table.Row key={`interest-${i}`}>
                    <Table.Cell width={3}>{getOrdinalSuffix(i + 1)}</Table.Cell>
                    <Table.Cell width={9}>{`${interestRate}% ${interestDetail}`}</Table.Cell>
                    <Table.Cell width={4}>${formatNumber(monthlyLoan[i + 1])}</Table.Cell>
                </Table.Row>
            );
        }
        return dom;
    }

    render() {
        const {
            bank: { rateName, bankImageUrl, rateTypeName, lockInPeriod, interestRates },
            calculatedData: {
                principalAmount,
                interestAmount,
                monthlyLoan,
                percentage,
                totalRepaymentAmount
            }
        } = this.props;

        return (
            <Segment className="bank-segment">
                <Grid>
                    <Grid.Row onClick={this.toggleVisibility}>
                        <Grid.Column width={5}>
                            <Image src={bankImageUrl} />
                        </Grid.Column>
                        <Grid.Column width={11}>
                            <Segment className="bank-segment__segment">
                                <Grid relaxed textAlign="left">
                                    <Grid.Row columns={1}>
                                        <Grid.Column
                                            className="bank-segment__name"
                                            textAlign="left"
                                        >
                                            {rateName}
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row columns={2}>
                                        <Grid.Column
                                            className="bank-segment__detail"
                                            floated="left"
                                        >
                                            Interest rate {interestRates[0]}%
                                        </Grid.Column>
                                        <Grid.Column
                                            className="bank-segment__detail"
                                            floated="right"
                                        >
                                            <strong>${formatNumber(monthlyLoan[1])} / month</strong>
                                        </Grid.Column>
                                    </Grid.Row>
                                    <Grid.Row>
                                        <Grid.Column
                                            verticalAlign="bottom"
                                            width={5}
                                            className="bank-segment__detail"
                                        >
                                            Principal <br />{' '}
                                            <strong>${formatNumber(principalAmount[1])}</strong>
                                        </Grid.Column>
                                        <Grid.Column
                                            verticalAlign="middle"
                                            width={6}
                                            className="bank-segment__detail"
                                        >
                                            <Progress
                                                percent={percentage[1]}
                                                color="blue"
                                                size="small"
                                                className="bank-segment__progress"
                                            />
                                        </Grid.Column>
                                        <Grid.Column
                                            verticalAlign="middle"
                                            width={5}
                                            className="bank-segment__detail"
                                        >
                                            Interest <br />{' '}
                                            <strong>${formatNumber(interestAmount[1])}</strong>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Segment>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row textAlign="left">
                        <Grid.Column width={8}>
                            <strong>Rate Type:</strong>
                        </Grid.Column>
                        <Grid.Column width={8}>{rateTypeName}</Grid.Column>
                        <Grid.Column width={8}>
                            <strong>Lock-in Period:</strong>
                        </Grid.Column>
                        <Grid.Column width={8}>{lockInPeriod}</Grid.Column>
                        <Grid.Column width={8}>
                            <strong>Total Repayment:</strong>
                        </Grid.Column>
                        <Grid.Column width={8}>${formatNumber(totalRepaymentAmount)}</Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Table striped unstackable textAlign="center" size="small">
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell width={3}>Year</Table.HeaderCell>
                                        <Table.HeaderCell width={9}>Interest Rate</Table.HeaderCell>
                                        <Table.HeaderCell width={4}>
                                            Monthly Repayment
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>

                                <Table.Body>{this.buildTableBody()}</Table.Body>
                            </Table>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column>
                            <Button color="orange" fluid>
                                Request Callback
                            </Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        );
    }
}

BankSegment.defaultProps = {
    bank: {},
    interestRatesDetails: [],
    calculatedData: {}
};

BankSegment.propTypes = {
    bank: PropTypes.oneOfType([PropTypes.object]),
    interestRatesDetails: PropTypes.oneOfType([PropTypes.array]),
    calculatedData: PropTypes.oneOfType([PropTypes.object])
};

export default BankSegment;
