import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    getBanksData,
    getCalculatedData
} from 'mcscreens/MortgageCalculator/MortgageCalculator.selectors';
import BankSegment from './components/BankSegment';
import './BankListing.scss';

class BanksListing extends Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        const { banks, calculatedData } = this.props;
        if (Object.keys(calculatedData).length === 0) {
            return null;
        }
        return (
            <div className="bank-listing">
                {banks.map(bank => {
                    return (
                        <BankSegment
                            bank={bank}
                            key={`bank-${bank._id}`}
                            calculatedData={calculatedData[bank._id]}
                        />
                    );
                })}
            </div>
        );
    }
}

BanksListing.defaultProps = {
    banks: [],
    calculatedData: {}
};

BanksListing.propTypes = {
    banks: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
    calculatedData: PropTypes.oneOfType([PropTypes.object])
};

const mapStateToProps = state => ({
    banks: getBanksData(state),
    calculatedData: getCalculatedData(state)
});

export default connect(mapStateToProps)(BanksListing);
