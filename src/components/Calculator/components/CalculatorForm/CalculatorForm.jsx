import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Button } from 'semantic-ui-react';
import CurrencyInput from 'react-currency-input';
import { reduxForm, SubmissionError } from 'redux-form';
import calculations from 'mcutils/calculations';
import reduxFormErrorMapper from 'mcutils/reduxFormErrorMapper';
import removeNumberFormat from 'mcutils/removeNumberFormat';
import tenures from 'mcdata/tenures';
import { actions as calculatorActions } from 'mcscreens/MortgageCalculator/MortgageCalculator.reducer';
import { getBanksData } from 'mcscreens/MortgageCalculator/MortgageCalculator.selectors';
import Hidden from 'mccomponents/Hidden';

import './CalculatorForm.scss';

class CalculatorForm extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {
            selectedTenure: 0,
            tenureYears: 0,
            propertyPrice: 450000, // Default value of $450,000
            loanAmount: 450000 * 0.8 // 80% of $450,000 property price
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleTenureClick = this.handleTenureClick.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit() {
        // I usually do this inside a redux-form with Field and everything but due to the time constraint and holidays, I ended up doing this
        const { tenureYears, loanAmount } = this.state;
        const { dispatch, banks } = this.props;

        const formData = {};
        Object.keys(banks).forEach(key => {
            const monthlyLoan = {};
            const interestAmount = {};
            const principalAmount = {};
            const percentage = {};
            let totalRepaymentAmount = 0;
            // eslint-disable-next-line no-plusplus
            for (let i = 0; i < tenureYears; i++) {
                const { interestRates } = banks[key];
                let interestRate = 0;
                if (i < interestRates.length) {
                    interestRate = interestRates[i];
                } else {
                    interestRate = interestRates[interestRates.length - 1]; // Get the last interest rate for remaining months
                }
                const r = calculations.getInterestRates(interestRate);
                const monthlyLoanDenom = calculations.getMonthlyLoanDenom(r, tenureYears);
                const monthlyLoanNomin = calculations.getMonthlyLoanNomin(r, tenureYears);
                const monthlyLoanAmount = calculations.getMonthlyLoan(
                    loanAmount,
                    monthlyLoanNomin,
                    monthlyLoanDenom
                );
                monthlyLoan[i + 1] = monthlyLoanAmount;
                const interest = calculations.getInterestAmount(loanAmount, r);
                interestAmount[i + 1] = interest;
                const principal = calculations.getPrincipalAmount(monthlyLoanAmount, interest);
                principalAmount[i + 1] = principal;
                percentage[i + 1] = calculations.getPercentage(principal, monthlyLoanAmount);
                totalRepaymentAmount += monthlyLoanAmount * 12;
            }
            const index = banks[key]._id;
            formData[index] = {
                interestAmount,
                principalAmount,
                monthlyLoan,
                percentage,
                totalRepaymentAmount,
                tenureYears
            };
        });
        // Save to reducer state for selectors
        return new Promise((resolve, reject) =>
            dispatch({
                promise: { resolve, reject },
                type: calculatorActions.START_SAVE_CALCULATED_DATA,
                formData
            })
        ).catch(error => {
            throw new SubmissionError(reduxFormErrorMapper(error));
        });
    }

    handleChange = ({ target: { name, value } }) => {
        if (name === 'propertyPrice') {
            const loanAmount = (removeNumberFormat(value) * 0.8).toFixed(2); // Get the 80% assumed loan amount from property price
            this.setState({ loanAmount });
        }
        this.setState({ [name]: removeNumberFormat(value) });
    };

    handleTenureClick(val) {
        this.setState({ selectedTenure: val, tenureYears: tenures[val] });
    }

    render() {
        const { selectedTenure, propertyPrice, loanAmount, tenureYears } = this.state;
        const { handleSubmit } = this.props; // submitting, pristine, invalid,

        return (
            <Form
                form="CalculatorForm"
                className="calculator-form"
                onSubmit={handleSubmit(this.onSubmit)}
            >
                <Form.Group unstackable widths={2}>
                    <Form.Input
                        placeholder="Property Price"
                        label="Property Price:"
                        name="propertyPrice"
                        width={8}
                    >
                        <CurrencyInput
                            name="propertyPrice"
                            value={propertyPrice}
                            decimalSeparator="."
                            thousandSeparator=","
                            prefix="$"
                            onChangeEvent={this.handleChange}
                        />
                    </Form.Input>
                    <Form.Input
                        placeholder="Loan Amount"
                        label="Loan Amount:"
                        name="loanAmount"
                        width={8}
                    >
                        <CurrencyInput
                            name="loanAmount"
                            value={loanAmount}
                            decimalSeparator="."
                            thousandSeparator=","
                            prefix="$"
                            onChangeEvent={this.handleChange}
                        />
                    </Form.Input>
                </Form.Group>
                <div className="calculator-form__tenures field">
                    <label htmlFor="btngrp-tenures">Tenure:</label>
                    <Button.Group fluid compact size="mini" id="btngrp-tenures">
                        {Object.keys(tenures).map(key => (
                            <Button
                                type="button"
                                key={`tenure-${key}`}
                                id={`tenure-${key}`}
                                inverted={key === selectedTenure}
                                basic={key !== selectedTenure}
                                active={key === selectedTenure}
                                color={key === selectedTenure ? 'orange' : 'grey'}
                                onClick={() => this.handleTenureClick(key)}
                                className="calculator-form__tenures__btn"
                            >
                                {tenures[key]}
                            </Button>
                        ))}
                        <Hidden name="tenureYears" id="tenureYears" value={tenureYears} />
                    </Button.Group>
                </div>
                <Button fluid inverted color="orange" type="submit">
                    Recalculate
                </Button>
            </Form>
        );
    }
}

CalculatorForm = reduxForm({
    form: 'CalculatorForm',
    enableReinitialize: true
})(CalculatorForm);

CalculatorForm.defaultProps = {
    dispatch: () => {},
    banks: {}
};

CalculatorForm.propTypes = {
    dispatch: PropTypes.func,
    handleSubmit: PropTypes.func.isRequired,
    banks: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};

const mapStateToProps = state => ({
    banks: getBanksData(state)
});

export default connect(mapStateToProps)(CalculatorForm);
