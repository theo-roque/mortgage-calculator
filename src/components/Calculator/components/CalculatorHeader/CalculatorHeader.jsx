import React, { Component } from 'react';
import { Grid, Header, Image } from 'semantic-ui-react';
import ImgCalculator from 'mcassets/img/calculator.png';
import './CalculatorHeader.scss';

class CalculatorHeader extends Component {
    constructor(props) {
        super(props);
        this.props = props;
        this.state = {};
    }

    render() {
        return (
            <Grid columns={2} className="calculator-header">
                <Grid.Row>
                    <Grid.Column width={4}>
                        <Image src={ImgCalculator} />
                    </Grid.Column>
                    <Grid.Column width={12}>
                        <Header as="h5" textAlign="left">
                            <Header.Content>
                                Monthly Payment Calculator
                                <Header.Subheader>
                                    Increasing prosperity in our lives can be accomplished by having
                                    the right frame of mind.
                                </Header.Subheader>
                            </Header.Content>
                        </Header>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

export default CalculatorHeader;
