import React, { Component } from 'react';
import { Card } from 'semantic-ui-react';
import CalculatorHeader from './components/CalculatorHeader';
import CalculatorForm from './components/CalculatorForm';
import './Calculator.scss';

class Calculator extends Component {
    constructor(props) {
        super(props);
        this.props = props;
    }

    render() {
        return (
            <div className="calculator">
                <Card centered fluid>
                    <Card.Content>
                        <Card.Header>
                            <CalculatorHeader />
                        </Card.Header>
                    </Card.Content>
                    <Card.Content>
                        <CalculatorForm />
                    </Card.Content>
                </Card>
            </div>
        );
    }
}

export default Calculator;
