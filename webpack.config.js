const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./config/paths');

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.(js|jsx|mjs)$/,
                exclude: [/@babel(?:\/|\\{1,2})runtime/, /node_modules/],
                loader: 'babel-loader',
                options: {
                    babelrc: true,
                    configFile: false,
                    compact: false,
                    presets: [
                        ['babel-preset-react-app/dependencies', { helpers: true }],
                        '@babel/preset-env',
                        '@babel/preset-react'
                    ],
                    cacheDirectory: true,
                    cacheCompression: false,
                    sourceMaps: false,
                    plugins: ['@babel/plugin-proposal-class-properties']
                }
            },
            {
                test: /\.(jpe?g|gif|png|svg)$/i,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 10000
                        }
                    }
                ]
            }
        ]
    },
    resolve: {
        alias: paths.appAlias,
        extensions: ['.js', '.jsx']
    },
    devServer: {
        contentBase: './public',
        hot: true
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html'
        })
    ]
};
